$( document ).ready( function() {

  'use strict';

      $( ".prevent-submit" ).submit( function( e ) {

        e.preventDefault();

    } );


    // Contact form - ouput values

   $( '#submit_btn' ).on( 'click', validateContactForm );

   function validateContactForm() {

     var email = $( '#user_mail' ).val();

     var name = $( '#user_name' ).val();

     var password = $( '#user_password' ).val();

     var lastname = $( '#user_last_name' ).val();

     var meet = $( '#user_meet' ).val();

     if ( email != '' && name != '' && password != '' && lastname != '' && meet != '' ) {

            var emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            var resultMailtest = emailPattern.test(String( email ).toLowerCase());

            var password = $( '#user_password' ).val();

            var passwordPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;

            var resultPasswordtest = passwordPattern.test(String( password ));


            if ( resultMailtest ) {

                if ( ! resultPasswordtest ) {

                  $( '#modal_data_wrong' ).modal();

                  $( '#modal_name_wrong' ).html( $( '#user_name' ).val() );
                  $( '#modal_last_name_wrong' ).html( $( '#user_last_name' ).val() );
                  $( '#modal_meet_wrong' ).html( $( '#user_meet' ).val() );
                  $( '#modal_mail_wrong' ).html( $( '#user_mail' ).val() );

                  $( '#modal_password_wrong' ).html( '<span class="text-danger font-weight-bold">' + 'El formato usado es incorrecto. Tu contraseña debe tener al menos 8 caracteres, entre los cuales, debe haber al menos una letra mayuscula, otra minuscula y un número.' + '</span>' );

                  return;

                }

                $( '#modal_data' ).modal();

                $( '#modal_name' ).html( $( '#user_name' ).val() );
                $( '#modal_last_name' ).html( $( '#user_last_name' ).val() );
                $( '#modal_meet' ).html( $( '#user_meet' ).val() );
                $( '#modal_mail' ).html( $( '#user_mail' ).val() );
                $( '#modal_password' ).html( $( '#user_password' ).val() );

              } else {

                $( '#modal_data_wrong' ).modal();

                   $( '#modal_name_wrong' ).html( $( '#user_name' ).val() );
                   $( '#modal_last_name_wrong' ).html( $( '#user_last_name' ).val() );
                   $( '#modal_meet_wrong' ).html( $( '#user_meet' ).val() );

                  if ( ! resultPasswordtest ) {

                    $( '#modal_password_wrong' ).html( '<span class="text-danger font-weight-bold">' + 'El formato usado es incorrecto. Tu contraseña debe tener al menos 8 caracteres, entre los cuales, debe haber al menos una letra mayuscula, otra minuscula y un número.' + '</span>' );

                  } else {

                    $( '#modal_password_wrong' ).html( $( '#user_password' ).val() );

                  }

                  $( '#modal_mail_wrong' ).html( '<span class="text-danger font-weight-bold">' + 'El formato usado es incorrecto. Debes introducir un email válido, como por ejemplo: robertoeat91@gmail.com' + '</span>' );

              }

     } else {

       $( '#modal_data_empty' ).modal();

     }

   }


    $( '#submit_login' ).on( 'click', validateLoginAccess );

    function validateLoginAccess() {

    var email = $( '#login_mail' ).val();

    var password = $( '#password_login' ).val();


    if ( email != '' && password != '' ) {

          var mailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

          var passwordPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;

          var resultPasswordPatterntest = passwordPattern.test(String( password ));

          var resultMailtest = mailPattern.test(String( email ).toLowerCase());

           if ( resultMailtest ) {

             if ( ! resultPasswordPatterntest ) {

               $( '#modal_loggin_false' ).modal();

               $( '#modal_mail_loggin_false' ).html( $( '#login_mail' ).val() );

               $( '#modal_password_loggin_false' ).html( '<span class="text-danger font-weight-bold">' + 'Este es un formato incorrecto. Debes introducir una contraseña de al menos 8 caracteres, en donde debe haber al menos una letra mayuscula, otra minuscula y un número' + '</span>' );

               return;

             }

             $( '#modal_loggin_true' ).modal();

             $( '#modal_mail_loggin' ).html( $( '#login_mail' ).val() );
             $( '#modal_password_loggin' ).html( $( '#password_login' ).val() );


           } else {

             $( '#modal_loggin_false' ).modal();

             if ( ! resultPasswordPatterntest ) {

               $( '#modal_password_loggin_false' ).html( '<span class="text-danger font-weight-bold">' + 'Este es un formato incorrecto. Debes introducir una contraseña de al menos 8 caracteres, en donde debe haber al menos una letra mayuscula, otra minuscula y un número' + '</span>' );

             } else {

               $( '#modal_password_loggin_false' ).html( $( '#password_login' ).val() );

             }

             $( '#modal_mail_loggin_false' ).html( '<span class="text-danger font-weight-bold">' + 'Este es un formato incorrecto. Debes introducir un correo valido. Por ejemplo: tunombre@gmail.com' + '</span>' );

           }

    } else {

      $( '#modal_data_empty' ).modal();

    }

  }

} );
